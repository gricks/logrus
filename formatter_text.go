package logrus

import (
	"bytes"
	"time"
)

var _ Formatter = &TextFormatter{}

type TextFormatter struct{}

func (this *TextFormatter) Format(b *bytes.Buffer, opts *Options, l Level, t time.Time, color []byte, prefix string, message string) []byte {
	var tb [len(RFC3339NanoLayout) + 10]byte
	b.Write(t.AppendFormat(tb[:0], RFC3339NanoLayout))
	b.WriteByte('|')
	b.Write(l.Bytes(opts.Color))
	b.WriteByte('|')
	if opts.Caller {
		b.Write(caller())
		b.WriteByte('|')
	}
	if len(color) > 0 {
		b.Write([]byte("Color:"))
		b.Write(color)
		b.WriteByte('|')
	}
	if len(prefix) > 0 {
		b.WriteString(prefix)
		b.WriteByte('|')
	}
	b.WriteString(message)
	b.WriteByte('\n')
	return b.Bytes()
}
