package logrus

import (
	"bytes"
	"time"
)

const RFC3339NanoLayout = "2006-01-02T15:04:05.000"

type Formatter interface {
	Format(*bytes.Buffer, *Options, Level, time.Time, []byte, string, string) []byte
}
