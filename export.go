package logrus

var (
	std      = New()
	stdEntry = std.GetEntry()
)

func GetLogger() *Logger   { return std }
func SetLogger(l *Logger)  { std.SetLogger(l) }
func GetEntry() *Entry     { return std.GetEntry() }
func SetLevel(level Level) { std.SetLevel(level) }
func GetLevel() Level      { return std.GetLevel() }
func AddHook(hook Hook)    { std.AddHook(hook) }

func Debug(format string, args ...interface{}) { stdEntry.Debug(format, args...) }
func Info(format string, args ...interface{})  { stdEntry.Info(format, args...) }
func Warn(format string, args ...interface{})  { stdEntry.Warn(format, args...) }
func Error(format string, args ...interface{}) { stdEntry.Error(format, args...) }
func Crit(format string, args ...interface{})  { stdEntry.Crit(format, args...) }
func Fatal(format string, args ...interface{}) { stdEntry.Fatal(format, args...) }
