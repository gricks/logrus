package logrus

import "time"

type Hook interface {
	Fire(Level, time.Time, []byte, string, string)
}

type Hooks []Hook

func (hs *Hooks) Add(h Hook) {
	*hs = append(*hs, h)
}

func (hs Hooks) Fire(level Level, t time.Time, color []byte, prefix string, message string) {
	for _, h := range hs {
		h.Fire(level, t, color, prefix, message)
	}
}
