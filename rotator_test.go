package logrus

import (
	"testing"
	"time"
)

func shouldBeTrue(ok bool) {
	if !ok {
		panic(ok)
	}
}

func shouldBeFalse(ok bool) {
	if ok {
		panic(ok)
	}
}

func shouldEqual(s1, s2 string) {
	if s1 != s2 {
		panic(s1 + "  " + s2)
	}
}

func TestRotator(t *testing.T) {
	name := "test"
	date := time.Date(2020, time.January, 1, 0, 0, 0, 0, time.Local)

	var (
		fname string
		ok    bool
	)

	// once
	once := new(OnceRotator)
	fname, ok = once.Rotate(name, date)
	shouldEqual(fname, name+".log")
	shouldBeTrue(ok)
	_, ok = once.Rotate(name, date)
	shouldBeFalse(ok)

	// daily
	daily := new(DailyRotator)
	fname, ok = daily.Rotate(name, date)
	shouldEqual(fname, name+"_20200101.log")
	shouldBeTrue(ok)
	_, ok = daily.Rotate(name, date)
	shouldBeFalse(ok)
	fname, ok = daily.Rotate(name, date.AddDate(0, 0, 1))
	shouldEqual(fname, name+"_20200102.log")
	shouldBeTrue(ok)

	// hourly
	hourly := new(HourlyRotator)
	fname, ok = hourly.Rotate(name, date)
	shouldEqual(fname, name+"_2020010100.log")
	shouldBeTrue(ok)
	_, ok = hourly.Rotate(name, date)
	shouldBeFalse(ok)
	fname, ok = hourly.Rotate(name, date.Add(time.Hour))
	shouldEqual(fname, name+"_2020010101.log")
	shouldBeTrue(ok)
}
