package logrus

import "path/filepath"

type Options struct {
	Color      bool
	Caller     bool
	Level      int
	BufferSize int
	Filename   string
	Filepath   string
	Rotator    Rotator
	Formatter  Formatter
}

type Option func(*Options)

func newOptions(opts ...Option) *Options {
	opt := &Options{
		Color:      false,
		Caller:     true,
		Level:      int(DEBUG),
		BufferSize: 0,
		Filename:   "stdout",
		Filepath:   "",
		Rotator:    new(OnceRotator),
		Formatter:  new(TextFormatter),
	}

	for _, o := range opts {
		o(opt)
	}

	return opt
}

func WithColor(color bool) Option {
	return func(o *Options) {
		o.Color = color
	}
}

func WithCaller(caller bool) Option {
	return func(o *Options) {
		o.Caller = caller
	}
}

func WithLevel(level int) Option {
	return func(o *Options) {
		o.Level = level
	}
}

func WithBufferSize(size int) Option {
	return func(o *Options) {
		o.BufferSize = size
	}
}

func WithRotator(r Rotator) Option {
	return func(o *Options) {
		o.Rotator = r
	}
}

func WithFormatter(f Formatter) Option {
	return func(o *Options) {
		o.Formatter = f
	}
}

func WithFile(name string) Option {
	return func(o *Options) {
		absName, err := filepath.Abs(name)
		if err != nil {
			return
		}
		o.Filepath, o.Filename = filepath.Split(absName)
	}
}
