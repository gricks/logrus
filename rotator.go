package logrus

import (
	"fmt"
	"time"
)

type Rotator interface {
	Rotate(name string, t time.Time) (string, bool)
}

type OnceRotator struct {
	init bool
}

func (r *OnceRotator) Rotate(name string, t time.Time) (string, bool) {
	if !r.init {
		r.init = true
		return name + ".log", true
	}
	return "", false
}

type DailyRotator struct {
	d int
}

func (r *DailyRotator) Rotate(name string, t time.Time) (string, bool) {
	y, m, d := t.Date()
	if d != r.d {
		r.d = d
		return fmt.Sprintf("%s_%04d%02d%02d.log", name, y, m, d), true
	}
	return "", false
}

type HourlyRotator struct {
	d, h int
}

func (r *HourlyRotator) Rotate(name string, t time.Time) (string, bool) {
	h := t.Hour()
	y, m, d := t.Date()
	if d != r.d || h != r.h {
		r.d, r.h = d, h
		return fmt.Sprintf("%s_%04d%02d%02d%02d.log", name, y, m, d, h), true
	}
	return "", false
}
