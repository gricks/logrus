package logrus

import (
	"bytes"
	"fmt"
	"os"
	"sync"
	"time"
)

var buffers *sync.Pool

func init() {
	buffers = &sync.Pool{
		New: func() interface{} {
			return bytes.NewBuffer(
				make([]byte, 0, 256),
			)
		},
	}
}

type Entry struct {
	logger *Logger
	prefix string
	color  []byte
}

func NewEntry(logger *Logger) *Entry {
	return &Entry{logger: logger}
}

func (entry *Entry) Prefix() string { return entry.prefix }
func (entry *Entry) SetPrefix(prefix string) *Entry {
	entry.prefix = prefix
	return entry
}

func (entry *Entry) Color() string { return string(entry.color) }
func (entry *Entry) SetColor(c []byte) *Entry {
	if len(c) == 0 {
		entry.color = color()
	} else {
		entry.color = append([]byte{}, c...)
	}
	return entry
}

func (entry *Entry) log(level Level, message string) {
	logger := entry.logger

	t := time.Now()
	logger.hooks.Fire(level, t, entry.color, entry.prefix, message)
	logger.rotate(t)

	buffer := buffers.Get().(*bytes.Buffer)
	buffer.Reset()
	b := logger.opts.Formatter.Format(buffer, logger.opts, level, t, entry.color, entry.prefix, message)

	logger.mu.Lock()
	_, _ = logger.output.Write(b)
	logger.mu.Unlock()

	buffers.Put(buffer)
}

func (entry *Entry) Debug(format string, args ...interface{}) {
	if entry.logger.GetLevel() >= DEBUG {
		entry.log(DEBUG, fmt.Sprintf(format, args...))
	}
}

func (entry *Entry) Info(format string, args ...interface{}) {
	if entry.logger.GetLevel() >= INFO {
		entry.log(INFO, fmt.Sprintf(format, args...))
	}
}

func (entry *Entry) Warn(format string, args ...interface{}) {
	if entry.logger.GetLevel() >= WARN {
		entry.log(WARN, fmt.Sprintf(format, args...))
	}
}

func (entry *Entry) Error(format string, args ...interface{}) {
	if entry.logger.GetLevel() >= ERROR {
		entry.log(ERROR, fmt.Sprintf(format, args...))
	}
}

func (entry *Entry) Crit(format string, args ...interface{}) {
	if entry.logger.GetLevel() >= CRIT {
		entry.log(CRIT, fmt.Sprintf(format, args...))
	}
}

func (entry *Entry) Fatal(format string, args ...interface{}) {
	if entry.logger.GetLevel() >= FATAL {
		entry.log(FATAL, fmt.Sprintf(format, args...))
	}
	os.Exit(1)
}
