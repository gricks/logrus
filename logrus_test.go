package logrus

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
	"testing"
)

func countLine(t *testing.T, f string) int {
	b, err := ioutil.ReadFile(f + ".log")
	if err != nil {
		t.Fatal(err)
	}
	return bytes.Count(b, []byte{'\n'})
}

func TestLogrus(t *testing.T) {
	fname := "logrus_test"
	logger := New(
		WithFile(fname),
		WithRotator(new(OnceRotator)),
	)
	defer os.Remove(fname + ".log")

	n, m := 10, 100
	wait := sync.WaitGroup{}
	for i := 0; i < n; i++ {
		wait.Add(1)
		go func() {
			defer wait.Done()
			entry := logger.
				GetEntry().
				SetColor(nil).
				SetPrefix("prefix")
			for i := 0; i < m; i++ {
				entry.Info("text:%d", i)
			}
		}()
	}
	wait.Wait()

	logger.Close()

	count := countLine(t, fname)
	if count != n*m {
		t.Fatalf("invalid line:%d count:%d\n", n*m, count)
	}
}

func TestSetLogger(t *testing.T) {
	fname1 := "logrus_test1"
	logger1 := New(WithFile(fname1), WithRotator(new(OnceRotator)))
	defer os.Remove(fname1 + ".log")

	m := 100
	entry := logger1.
		GetEntry().
		SetColor(nil).
		SetPrefix("prefix")
	for i := 0; i < m; i++ {
		entry.Info("text:%d", i)
	}

	logger1.Close()

	count1 := countLine(t, fname1)
	if count1 != m {
		t.Fatalf("invalid line:%d count:%d\n", m, count1)
	}

	fname2 := "logrus_test2"
	logger2 := New(WithFile(fname2), WithRotator(new(OnceRotator)))
	defer os.Remove(fname2 + ".log")

	logger1.SetLogger(logger2)

	// reuse entry
	for i := 0; i < m; i++ {
		entry.Info("text:%d", i)
	}

	logger1.Close()

	count2 := countLine(t, fname1)
	if count2 != m {
		t.Fatalf("invalid line:%d count:%d\n", m, count2)
	}
}

func TestAdapter(t *testing.T) {
	fname1 := "logrus_test3"
	logger1 := New(WithFile(fname1), WithRotator(new(OnceRotator)))
	defer os.Remove(fname1 + ".log")

	m := 100
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		entry := logger1.
			GetEntry().
			SetColor(nil).
			SetPrefix("prefix")
		for i := 0; i < m; i++ {
			entry.Info("text:%d", i)
		}
	}()

	stdLogger := logger1.GetStdLogger("", 0)
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < m; i++ {
			stdLogger.Print("ok")
		}
	}()

	wg.Wait()

	logger1.Close()

	count1 := countLine(t, fname1)
	if count1 != m*2 {
		t.Fatalf("invalid line:%d count:%d\n", m*2, count1)
	}
}

func BenchmarkDummyLogger(b *testing.B) {
	logger := New(WithFile("logrus_dummy"))
	defer logger.Close()
	entry := logger.GetEntry().SetPrefix("alsdfjalsf sdkfjaslkdfj")

	b.StopTimer()
	b.StartTimer()
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			entry.Info("aaa")
		}
	})
}

func BenchmarkDummyLoggerDiscard(b *testing.B) {
	logger := New()
	defer logger.Close()
	logger.output = ioutil.Discard
	entry := logger.GetEntry().SetPrefix("alsdfjalsf sdkfjaslkdfj")

	b.StopTimer()
	b.StartTimer()
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			entry.Info("aaa")
		}
	})
}

func benchmarkWithFeature(b *testing.B, opts ...Option) {
	logger := New(opts...)
	defer logger.Close()

	entry := logger.GetEntry().
		SetColor(nil).
		SetPrefix(fmt.Sprintf("UID:%d|GID:%d|Command:%s", 10000, 1, "GET_USER_INFO"))

	b.StopTimer()
	b.StartTimer()

	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			entry.Info("aaa")
		}
	})
}

func BenchmarkLoggerWithFeature(b *testing.B) {
	benchmarkWithFeature(b,
		WithFile("logrus_feature"),
		WithCaller(true),
		WithRotator(new(DailyRotator)))
}

func BenchmarkLoggerWithBufio4096(b *testing.B) {
	benchmarkWithFeature(b,
		WithFile("logrus_bufio_4096"),
		WithCaller(true),
		WithRotator(new(DailyRotator)),
		WithBufferSize(4096))
}

func BenchmarkLoggerWithBufio10240(b *testing.B) {
	benchmarkWithFeature(b,
		WithFile("logrus_bufio_10240"),
		WithCaller(true),
		WithRotator(new(DailyRotator)),
		WithBufferSize(10240))
}
