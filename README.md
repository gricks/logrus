# Logrus
Logrus is a structured logger for golang.

# Benchmark
```
go test -test.bench=".*"
goos: linux
goarch: amd64
pkg: gitee.com/gricks/logrus
BenchmarkDummyLogger-4                    242300          4194 ns/op
BenchmarkDummyLoggerDiscard-4            2038996           624 ns/op
BenchmarkLoggerWithFeature-4              319626          4164 ns/op
BenchmarkLoggerWithBufio4096-4            627020          1736 ns/op
BenchmarkWrapperLoggerBufio10240-4        755038          1636 ns/op
```
