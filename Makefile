benchmark:
	go test -test.bench=".*" -cpuprofile=cpu.prof 
	rm -rf *.log

race:
	go test --race
	rm -rf *.log
