package logrus

import (
	"io"
	"log"
	"sync"
)

type lockedWriter struct {
	mu  *sync.Mutex
	out io.Writer
}

func (w *lockedWriter) Write(p []byte) (n int, err error) {
	w.mu.Lock()
	defer w.mu.Unlock()
	return w.out.Write(p)
}

func (l *Logger) GetStdLogger(prefix string, flag int) *log.Logger {
	l.mu.Lock()
	defer l.mu.Unlock()
	return log.New(&lockedWriter{mu: &l.mu, out: l.output},
		prefix, flag)
}
