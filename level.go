package logrus

type Level uint32

const (
	FATAL = Level(0)
	CRIT  = Level(1)
	ERROR = Level(2)
	WARN  = Level(3)
	INFO  = Level(4)
	DEBUG = Level(5)
)

var lstrs = [...]string{
	"FATL",
	"CRIT",
	"EROR",
	"WARN",
	"INFO",
	"DEBG",
}

var lbytes = [...][]byte{
	[]byte(lstrs[0]),
	[]byte(lstrs[1]),
	[]byte(lstrs[2]),
	[]byte(lstrs[3]),
	[]byte(lstrs[4]),
	[]byte(lstrs[5]),
}

var lbytesColor = [...][]byte{
	[]byte("\x1b[1;91m" + lstrs[0] + "\x1b[0m"),
	[]byte("\x1b[1;91m" + lstrs[1] + "\x1b[0m"),
	[]byte("\x1b[1;91m" + lstrs[2] + "\x1b[0m"),
	[]byte("\x1b[1;93m" + lstrs[3] + "\x1b[0m"),
	[]byte("\x1b[1;92m" + lstrs[4] + "\x1b[0m"),
	[]byte("\x1b[1;94m" + lstrs[5] + "\x1b[0m"),
}

func (l Level) String() string {
	return lstrs[l]
}

func (l Level) Bytes(color bool) []byte {
	if color {
		return lbytesColor[l]
	}
	return lbytes[l]
}
