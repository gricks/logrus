package logrus

import (
	"bytes"
	"time"

	jsoniter "github.com/json-iterator/go"
)

var _ Formatter = &JSONFormatter{}

type JSONFormatter struct{}

func (this *JSONFormatter) Format(b *bytes.Buffer, opts *Options, l Level, t time.Time, color []byte, prefix string, message string) []byte {
	data := make(map[string]interface{}, 4)
	data["level"] = l.String()
	data["time"] = t.Format(RFC3339NanoLayout)
	if opts.Caller {
		data["caller"] = string(caller())
	}
	if len(color) > 0 {
		data["color"] = string(color)
	}
	if len(prefix) > 0 {
		data["prefix"] = prefix
	}
	data["msg"] = message
	bs, _ := jsoniter.Marshal(data)
	return append(bs, '\n')
}
