package logrus

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path"
	"runtime"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type Logger struct {
	mu       sync.Mutex
	writer   *os.File
	syncer   *bufio.Writer
	output   io.Writer
	hooks    Hooks
	level    Level
	opts     *Options
	rotating int32
}

func New(opts ...Option) *Logger {
	logger := &Logger{
		opts: newOptions(opts...),
	}
	logger.SetLevel(Level(logger.opts.Level))
	logger.rotate(time.Now())
	return logger
}

func (l *Logger) SetLogger(n *Logger) *Logger {
	l.mu.Lock()
	defer l.mu.Unlock()

	l.Close()
	return l.clone(n)
}

func (l *Logger) GetEntry() *Entry {
	return NewEntry(l)
}

func (l *Logger) GetLevel() Level {
	return Level(atomic.LoadUint32((*uint32)(&l.level)))
}

func (l *Logger) SetLevel(level Level) {
	atomic.StoreUint32((*uint32)(&l.level), uint32(level))
}

func (l *Logger) AddHook(hook Hook) {
	l.mu.Lock()
	l.hooks.Add(hook)
	l.mu.Unlock()
}

func (l *Logger) Close() {
	if l.syncer != nil {
		l.syncer.Flush()
	}
	if l.writer != nil {
		l.writer.Close()
	}
}

func (l *Logger) rotate(t time.Time) {
	// Make sure it's not parallel
	if !atomic.CompareAndSwapInt32(
		&l.rotating, 0, 1) {
		return
	}

	defer atomic.StoreInt32(&l.rotating, 0)
	fname, ok := l.opts.Rotator.Rotate(
		l.opts.Filename, t)
	if !ok {
		return
	}

	// stdout & stderr must be OnceRotator
	switch strings.ToLower(l.opts.Filename) {
	case "stdout":
		l.output = os.Stdout
		return
	case "stderr":
		l.output = os.Stderr
		return
	}

	writer, err := os.OpenFile(path.Join(l.opts.Filepath, fname),
		os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		fmt.Fprintf(os.Stderr, "create file(%s) failed with error(%v)\n",
			path.Join(l.opts.Filepath, fname), err)
		return
	}

	l.mu.Lock()
	defer l.mu.Unlock()

	l.Close()
	l.writer = writer

	// with buffer
	if l.opts.BufferSize > 0 {
		l.syncer = bufio.NewWriterSize(l.writer, l.opts.BufferSize)
		l.output = l.syncer
	} else {
		l.output = l.writer
	}
}

func (l *Logger) clone(n *Logger) *Logger {
	l.SetLevel(n.level)
	l.opts = n.opts
	l.hooks = n.hooks
	l.writer = n.writer
	l.syncer = n.syncer
	l.output = n.output
	return l
}

func caller() []byte {
	_, file, line, ok := runtime.Caller(4)
	if !ok {
		file = "x"
		line = 0
	}
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			file = file[i+1:]
			break
		}
	}

	bs := make([]byte, 0, len(file)+20)
	bs = append(bs, file...)
	var b [20]byte
	pos := len(b) - 1
	for line >= 10 {
		q := line / 10
		b[pos] = byte('0' + line - q*10)
		pos--
		line = q
	}
	b[pos] = byte('0' + line)
	b[pos-1] = byte(':')
	bs = append(bs, b[pos-1:]...)
	return bs
}
