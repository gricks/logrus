package logrus

import (
	"math/rand"
	"sync"
	"time"
)

var (
	rd = rand.NewSource(time.Now().UnixNano())
	mu = sync.Mutex{}
	tb = []byte("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZxX") // len 64
)

func color() []byte {
	mu.Lock()
	v := rd.Int63()
	mu.Unlock()
	b := make([]byte, 10)
	for i := 0; i < 10; i++ {
		b[i] = tb[v&0x3F]
		v >>= 6
	}
	return b
}
